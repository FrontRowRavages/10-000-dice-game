import java.util.Random;
import java.util.Scanner;

/**
 * Created by colinhill + andreidogaru on 9/16/15.
 */
public class DiceGame10000 {

        public static void main(String[] args){
            Scanner console = new Scanner(System.in);

            int totalScore = 0;

            intro();

            for (int i = 1; i <= 10; i++){

                totalScore += startRound(console);
                System.out.printf("Your Total score after Round %d is %d \n",i, totalScore);

            }


        }

        public static void intro() {
            //Introduce the game to the user
            System.out.println("Welcome to 10,000, this is a 1 Player version that where ");
            System.out.println("you have 10 rounds to get the highest score possible.  ");
            System.out.println();
            System.out.println("RULES: ");
            System.out.println("If you are not familiar with the game, do a Search for 10,000.");
            System.out.println();
            System.out.println("If you score all of your Dice, then enter 6 to keep throwing ");
            System.out.println();
            System.out.println("If you gamble and you do not have any scoring dice, enter 0 to");
            System.out.println("start the next round and try again");
            System.out.println();
            System.out.println("Please type as the program instructs, and do not be clever and ruin");
            System.out.println("the fun of this game. We are only 3 weeks into Programming in Java");
            System.out.println();
            System.out.println("Enjoy your game :)");
            System.out.println();


        }

        public static int startRound(Scanner console) {
            int numberOfDice = 6, potentialScore = 0, roundScore = 0;

            //Go to RollDice() and roll 6 dice
            // RollDice() updates Potential score
            potentialScore = rollDice(numberOfDice);

            System.out.printf("The Potential Score for this throw is %d \n", potentialScore);

            //Prompt user SaveOrGamble
            System.out.print("Would you like to Save or Gamble ");
            String saveOrGamble = console.nextLine();

            roundScore += potentialScore;

            if (saveOrGamble.equalsIgnoreCase("")){

                saveOrGamble = console.nextLine();
            }

            if (saveOrGamble.equalsIgnoreCase("Gamble") && potentialScore > 0) {
                //if User selects Gamble go to Gamble
                //RollDice() = remaining dice
                //Enters loop for round as long as PotentialScore > 0 && User enters Gamble



                System.out.print("Throw the number of nonscoring dice ");
                int diceToThrow = console.nextInt();


                while(diceToThrow > 0 && potentialScore >0) {

                    potentialScore = rollDice(diceToThrow);
                    System.out.printf("The Potential Score for this throw is %d \n", potentialScore);

                    roundScore += potentialScore;

                    System.out.print("Throw the number of nonscoring dice ");
                    diceToThrow = console.nextInt();

                }

                if (potentialScore == 0) {
                    return 0;
                }





            } else if (saveOrGamble.equalsIgnoreCase("Save")) {
                //if User selects save, go to save method to update points

                return roundScore;
            }else {
                return 0;
            }


            //IF potentialScore == 0, go to next round, and do not update TOTAL Score
            return roundScore;
        }


        public static void gamble(Scanner console){
            //Ask user how many dice he would like to save


            //Add those dice to the round score


        }



        public static int getDiceValue() {
            // Enter dice random characteristics
            Random d = new Random();
            return d.nextInt(6)+1;

        }

        public static int rollDice(int numberOfDice) {

            //Roll a number of dice declared by the user
            int sum = 0;

            int[] values = new int[(7)];
            for (int i = 1; i <= 6; i++) {
                values[i] = 0;

            }

            for (int i = 1; i <= numberOfDice; i++) {
                int x = getDiceValue();
                values[x]++;
                System.out.printf("Dice #%d = %d \n", i, x);
            }

            //Call Scoring Methods to scoring methods to get potenitalScore
            int initSum = sum;
            int sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
            sum += checkForStraight(values,sum1);
            if(initSum == sum){
                sum += checkFor3Pairs(values,sum2);
                if(initSum == sum){
                    sum += checkForGeneralScoring(values,sum3);
                    sum += checkFor1(values,sum4);
                }

            }

            // Return the Sum from the Roll == Potential Score
            return sum;

        }


        public static int checkForStraight(int[] values, int sum){

            int flag=1;
            for(int i = 1; i <= 6;i++)
                if(values[i]==1)
                    flag=1;
                else{
                    flag=0;
                    break;
                }
            if(flag==1)
                sum+=2000;
            return sum;
        }

        public static int checkFor3Pairs(int[] values, int sum){
            int flag = 0;
            for(int i = 1; i <= 6;i++)
                if(values[i]==2)
                    flag++;
            if(flag==3)
                sum+=1500;
            return sum;
        }

        public static int checkForGeneralScoring(int[] values, int sum){
            // Get score for 5 when you roll 1 and 2 fives
            if(values[5]==1 || values[5]==2)
                sum+=50*values[5];

            //General Scoring for number 2, 3, 4, 5, 6
            for(int i = 2; i <= 6;i++){
                if(values[i]==3)
                    sum+=i*100;
                else if(values[i]==4)
                    sum+=i*200;
                else if(values[i]==5)
                    sum+=i*300;
                else if(values[i]==6)
                    sum+=i*400;
            }
            return sum;
        }

        public static int checkFor1(int[] values, int sum){
            //check values for die == 1
            if(values[1]==1 || values[1]==2)
                sum+=100*values[1];
            if(values[1]==3)
                sum+=1000;
            if(values[1]==4)
                sum+=2000;
            if(values[1]==5)
                sum+=4000;
            if(values[1]==6)
                sum+=8000;
            return sum;
        }

      
    public static void checkForZeroPoints(int numberOfDice) {
        int sum = 0;

        // If Score = 0 after they roll all 6 dice then we have to let the user know
      if (sum == 0 && numberOfDice == 6) {
            System.out.println();
            System.out.println("That's a Hans Curse! DRINK!!!!");

            // If the score of a roll is Zero then we have to void the sum
        }
        if (sum == 0 && numberOfDice < 6) {
            System.out.println();
            System.out.println("Start with 6 dice and throw again! And you should Probably Drink");

        }
        System.out.println();
    }



}
